/**
 * Created by alex on 26/04/16.
 */
var avenuecodegeolocation = require('../../../server.js');

require('./bootstrap.js');

// Helpers Tests
require('./steps/helpers_spec.js')(avenuecodegeolocation.app);

// Crypter Tests
require('./steps/crypter_spec.js')(avenuecodegeolocation.app);

// Users Tests
require('./steps/users_spec.js')(avenuecodegeolocation.app);

// Auth Tests
require('./steps/auth_spec.js')(avenuecodegeolocation.app);

// Auth Tests
require('./steps/favoriteLocation_spec.js')(avenuecodegeolocation.app);
