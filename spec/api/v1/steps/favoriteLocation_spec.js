/**
 * Created by alex on 27/04/16.
 */
var request = require('supertest');
var expect = require('chai').expect;

var global = require('../../../helpers/global-v1');

var favoriteLocationTest = global['favoriteLocationTest'];
var userTest3 = global['userTest3'];

module.exports = function(app) {

    describe('List, put and delete a favorite location', function () {

        this.timeout(50000000);

        var favoriteLocation;
        var user;
        var authId;

        it('[teste]\t POST /api/v1/user', function (done) {

            request(app)
                .post('/api/v1/user')
                .send(userTest3)
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        console.log(JSON.stringify(err, null, 3));
                        done(err);
                    }
                    else {
                        var json = res.body;
                        //console.log(JSON.stringify(json, null, 3));

                        expect(json).to.be.a('object');
                        expect(json.createdAt).to.be.a('string');
                        expect(json.updatedAt).to.be.a('string');
                        expect(json.id).to.be.a('string');
                        expect(json.name).to.be.a('string');
                        expect(json.surname).to.be.a('string');
                        expect(json.email).to.be.a('string');
                        expect(json.username).to.be.a('string');

                        expect(json.name).to.equal(userTest3.name);
                        expect(json.surname).to.equal(userTest3.surname);
                        expect(json.email).to.equal(userTest3.email);
                        expect(json.username).to.equal(userTest3.username);

                        user = json;

                        done();
                    }
                });
        });

        it('[teste]\t GET /api/v1/user/:id', function (done) {

            request(app)
                .get('/api/v1/user/'+user.id)
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        console.log(JSON.stringify(err, null, 3));
                        done(err);
                    }
                    else {
                        var json = res.body;
//                        console.log(JSON.stringify(json, null, 3));

                        expect(json).to.be.a('object');
                        expect(json.createdAt).to.be.a('string');
                        expect(json.updatedAt).to.be.a('string');
                        expect(json.id).to.be.a('string');
                        expect(json.name).to.be.a('string');
                        expect(json.surname).to.be.a('string');
                        expect(json.email).to.be.a('string');
                        expect(json.username).to.be.a('string');

                        expect(json.name).to.equal(userTest3.name);
                        expect(json.surname).to.equal(userTest3.surname);
                        expect(json.email).to.equal(userTest3.email);
                        expect(json.username).to.equal(userTest3.username);

                        user = json;

                        done();
                    }
                });
        });

        it('[teste]\t PUT /api/v1/favoriteLocation: error because user is not logged', function (done) {

            favoriteLocationTest.user = user.id;

            request(app)
                .put('/api/v1/favoriteLocation/')
                .send(favoriteLocationTest)
                .expect('Content-Type', /json/)
                .expect(403)
                .end(function (err, res) {
                    if (err) {
                        console.log(JSON.stringify(err, null, 3));
                        done(err);
                    }
                    else {
                        // var json = res.body;
                        // console.log(JSON.stringify(json, null, 3));

                        done();
                    }
                });
        });

        it('[teste]\t POST /api/v1/auth/login', function (done) {

            request(app)
                .post('/api/v1/auth/login')
                .send({
                    username : userTest3.username,
                    password : userTest3.password
                })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        console.log(JSON.stringify(err, null, 3));
                        done(err);
                    }
                    else {
                        var json = res.body;
                        // console.log(JSON.stringify(json, null, 3));

                        expect(json.user).to.be.a('object');
                        expect(json.createdAt).to.be.a('string');
                        expect(json.updatedAt).to.be.a('string');
                        expect(json.user.id).to.be.a('string');
                        expect(json.user.name).to.be.a('string');
                        expect(json.user.surname).to.be.a('string');
                        expect(json.user.email).to.be.a('string');
                        expect(json.user.username).to.be.a('string');

                        expect(json.user.name).to.equal(userTest3.name);
                        expect(json.user.username).to.equal(userTest3.username);
                        expect(json.user.email).to.equal(userTest3.email);
                        expect(json.user.username).to.equal(userTest3.username);
                        expect(json.user.id).to.equal(user.id);

                        authId = json.id;

                        done();
                    }
                })

        });

        it('[teste]\t PUT /api/v1/favoriteLocation: it is logged', function (done) {

            favoriteLocationTest.user = user.id;

            request(app)
                .put('/api/v1/favoriteLocation/')
                .send(favoriteLocationTest)
                .set('x-auth', authId)
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        console.log(JSON.stringify(err, null, 3));
                        done(err);
                    }
                    else {
                        var json = res.body;
//                        console.log(JSON.stringify(json, null, 3));

                        expect(json).to.be.a('object');
                        expect(json.createdAt).to.be.a('string');
                        expect(json.updatedAt).to.be.a('string');
                        expect(json.id).to.be.a('string');
                        expect(json.user).to.be.a('string');
                        expect(json.as).to.be.a('string');
                        expect(json.city).to.be.a('string');
                        expect(json.country).to.be.a('string');
                        expect(json.countryCode).to.be.a('string');
                        expect(json.isp).to.be.a('string');
                        expect(json.lat).to.be.a('number');
                        expect(json.lon).to.be.a('number');
                        expect(json.org).to.be.a('string');
                        expect(json.query).to.be.a('string');
                        expect(json.region).to.be.a('string');
                        expect(json.regionName).to.be.a('string');
                        expect(json.status).to.be.a('string');
                        expect(json.timezone).to.be.a('string');
                        expect(json.zip).to.be.a('string');
                        expect(json.hostname).to.be.a('string');

                        expect(json.user).to.equal(favoriteLocationTest.user);
                        expect(json.as).to.equal(favoriteLocationTest.as);
                        expect(json.city).to.equal(favoriteLocationTest.city);
                        expect(json.country).to.equal(favoriteLocationTest.country);
                        expect(json.countryCode).to.equal(favoriteLocationTest.countryCode);
                        expect(json.isp).to.equal(favoriteLocationTest.isp);
                        expect(json.lat).to.equal(favoriteLocationTest.lat);
                        expect(json.lon).to.equal(favoriteLocationTest.lon);
                        expect(json.org).to.equal(favoriteLocationTest.org);
                        expect(json.query).to.equal(favoriteLocationTest.query);
                        expect(json.region).to.equal(favoriteLocationTest.region);
                        expect(json.regionName).to.equal(favoriteLocationTest.regionName);
                        expect(json.status).to.equal(favoriteLocationTest.status);
                        expect(json.timezone).to.equal(favoriteLocationTest.timezone);
                        expect(json.zip).to.equal(favoriteLocationTest.zip);
                        expect(json.hostname).to.equal(favoriteLocationTest.hostname);

                        favoriteLocation = json;

                        done();
                    }
                });
        });

        it('[teste]\t GET /api/v1/favoriteLocation: it is logged but without userId', function (done) {

            request(app)
                .get('/api/v1/favoriteLocation')
                .set('x-auth', authId)
                .expect('Content-Type', /json/)
                .expect(403)
                .end(function (err, res) {
                    if (err) {
                        console.log(JSON.stringify(err, null, 3));
                        done(err);
                    }
                    else {
                        // var json = res.body;
//                        console.log(JSON.stringify(json, null, 3));

                        done();
                    }
                });
        });

        it('[teste]\t GET /api/v1/favoriteLocation: it is logged with userId', function (done) {

            request(app)
                .get('/api/v1/favoriteLocation?userId='+user.id)
                .set('x-auth', authId)
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        console.log(JSON.stringify(err, null, 3));
                        done(err);
                    }
                    else {
                        var json = res.body;
//                        console.log(JSON.stringify(json, null, 3));

                        expect(json).to.be.a('array');
                        expect(json[0].createdAt).to.be.a('string');
                        expect(json[0].updatedAt).to.be.a('string');
                        expect(json[0].id).to.be.a('string');
                        expect(json[0].user).to.be.a('string');
                        expect(json[0].as).to.be.a('string');
                        expect(json[0].city).to.be.a('string');
                        expect(json[0].country).to.be.a('string');
                        expect(json[0].countryCode).to.be.a('string');
                        expect(json[0].isp).to.be.a('string');
                        expect(json[0].lat).to.be.a('number');
                        expect(json[0].lon).to.be.a('number');
                        expect(json[0].org).to.be.a('string');
                        expect(json[0].query).to.be.a('string');
                        expect(json[0].region).to.be.a('string');
                        expect(json[0].regionName).to.be.a('string');
                        expect(json[0].status).to.be.a('string');
                        expect(json[0].timezone).to.be.a('string');
                        expect(json[0].zip).to.be.a('string');
                        expect(json[0].hostname).to.be.a('string');

                        expect(json[0].user).to.equal(favoriteLocationTest.user);
                        expect(json[0].as).to.equal(favoriteLocationTest.as);
                        expect(json[0].city).to.equal(favoriteLocationTest.city);
                        expect(json[0].country).to.equal(favoriteLocationTest.country);
                        expect(json[0].countryCode).to.equal(favoriteLocationTest.countryCode);
                        expect(json[0].isp).to.equal(favoriteLocationTest.isp);
                        expect(json[0].lat).to.equal(favoriteLocationTest.lat);
                        expect(json[0].lon).to.equal(favoriteLocationTest.lon);
                        expect(json[0].org).to.equal(favoriteLocationTest.org);
                        expect(json[0].query).to.equal(favoriteLocationTest.query);
                        expect(json[0].region).to.equal(favoriteLocationTest.region);
                        expect(json[0].regionName).to.equal(favoriteLocationTest.regionName);
                        expect(json[0].status).to.equal(favoriteLocationTest.status);
                        expect(json[0].timezone).to.equal(favoriteLocationTest.timezone);
                        expect(json[0].zip).to.equal(favoriteLocationTest.zip);
                        expect(json[0].hostname).to.equal(favoriteLocationTest.hostname);

                        done();
                    }
                });
        });

        it('[teste]\t GET /api/v1/favoriteLocation/:id: it is logged', function (done) {

            request(app)
                .get('/api/v1/favoriteLocation/'+favoriteLocation.id)
                .set('x-auth', authId)
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        console.log(JSON.stringify(err, null, 3));
                        done(err);
                    }
                    else {
                        var json = res.body;
//                        console.log(JSON.stringify(json, null, 3));

                        expect(json).to.be.a('object');
                        expect(json.createdAt).to.be.a('string');
                        expect(json.updatedAt).to.be.a('string');
                        expect(json.id).to.be.a('string');
                        expect(json.user).to.be.a('string');
                        expect(json.as).to.be.a('string');
                        expect(json.city).to.be.a('string');
                        expect(json.country).to.be.a('string');
                        expect(json.countryCode).to.be.a('string');
                        expect(json.isp).to.be.a('string');
                        expect(json.lat).to.be.a('number');
                        expect(json.lon).to.be.a('number');
                        expect(json.org).to.be.a('string');
                        expect(json.query).to.be.a('string');
                        expect(json.region).to.be.a('string');
                        expect(json.regionName).to.be.a('string');
                        expect(json.status).to.be.a('string');
                        expect(json.timezone).to.be.a('string');
                        expect(json.zip).to.be.a('string');
                        expect(json.hostname).to.be.a('string');

                        expect(json.user).to.equal(favoriteLocationTest.user);
                        expect(json.as).to.equal(favoriteLocationTest.as);
                        expect(json.city).to.equal(favoriteLocationTest.city);
                        expect(json.country).to.equal(favoriteLocationTest.country);
                        expect(json.countryCode).to.equal(favoriteLocationTest.countryCode);
                        expect(json.isp).to.equal(favoriteLocationTest.isp);
                        expect(json.lat).to.equal(favoriteLocationTest.lat);
                        expect(json.lon).to.equal(favoriteLocationTest.lon);
                        expect(json.org).to.equal(favoriteLocationTest.org);
                        expect(json.query).to.equal(favoriteLocationTest.query);
                        expect(json.region).to.equal(favoriteLocationTest.region);
                        expect(json.regionName).to.equal(favoriteLocationTest.regionName);
                        expect(json.status).to.equal(favoriteLocationTest.status);
                        expect(json.timezone).to.equal(favoriteLocationTest.timezone);
                        expect(json.zip).to.equal(favoriteLocationTest.zip);
                        expect(json.hostname).to.equal(favoriteLocationTest.hostname);

                        done();
                    }
                });
        });

        it('[teste]\t DELETE /api/v1/favoriteLocation/:id: it is logged', function (done) {

            request(app)
                .delete('/api/v1/favoriteLocation/'+favoriteLocation.id)
                .set('x-auth', authId)
                .expect(204)
                .end(function (err, res) {
                    if (err) {
                        console.log(JSON.stringify(err, null, 3));
                        done(err);
                    }
                    else {
                        done();
                    }
                });
        });
    })
};
