/**
 * Created by alex on 26/04/16.
 */
var expect = require('chai').expect;

var helpers = require('../../../../server/utils/helpers');

module.exports = function() {
	describe('Verify email validity', function () {
		this.timeout(50000000);

		it('[teste]\t Verify email with correct value', function (done) {
			expect(helpers.verifyEmail('test@test.com')).to.be.a('boolean');

			expect(helpers.verifyEmail('test@test.com')).to.equal(true);

			done();
		});

		it('[teste]\t Verify email with wrong value', function (done) {
			expect(helpers.verifyEmail('test')).to.be.a('boolean');

			expect(helpers.verifyEmail('test')).to.equal(false);

			done();
		});

		it('[teste]\t Verify email with wrong value', function (done) {
			expect(helpers.verifyEmail('test@test')).to.be.a('boolean');

			expect(helpers.verifyEmail('test@test')).to.equal(false);

			done();
		});

	});
};
