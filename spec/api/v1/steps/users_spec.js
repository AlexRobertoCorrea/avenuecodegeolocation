/**
 * Created by alex on 26/04/16.
 */
var request = require('supertest');
var expect = require('chai').expect;

var global = require('../../../helpers/global-v1');

var userTest = global['userTest'];

module.exports = function(app) {

	describe('Create and list user', function () {

		this.timeout(50000000);

		var user;

		it('[teste]\t POST /api/v1/user', function (done) {

			request(app)
				.post('/api/v1/user')
				.send(userTest)
				.expect('Content-Type', /json/)
				.expect(200)
				.end(function (err, res) {
					if (err) {
						console.log(JSON.stringify(err, null, 3));
						done(err);
					}
					else {
						var json = res.body;
                        //console.log(JSON.stringify(json, null, 3));

						expect(json).to.be.a('object');
						expect(json.createdAt).to.be.a('string');
						expect(json.updatedAt).to.be.a('string');
						expect(json.id).to.be.a('string');
						expect(json.name).to.be.a('string');
						expect(json.surname).to.be.a('string');
						expect(json.email).to.be.a('string');
						expect(json.username).to.be.a('string');

						expect(json.name).to.equal(userTest.name);
						expect(json.surname).to.equal(userTest.surname);
						expect(json.email).to.equal(userTest.email);
						expect(json.username).to.equal(userTest.username);

                        user = json;

						done();
					}
				});
		});

		it('[teste]\t GET /api/v1/user/:id', function (done) {

			request(app)
				.get('/api/v1/user/'+user.id)
				.expect('Content-Type', /json/)
				.expect(200)
				.end(function (err, res) {
					if (err) {
						console.log(JSON.stringify(err, null, 3));
						done(err);
					}
					else {
						var json = res.body;
//                        console.log(JSON.stringify(json, null, 3));

                        expect(json.createdAt).to.be.a('string');
                        expect(json.updatedAt).to.be.a('string');
                        expect(json.id).to.be.a('string');
                        expect(json.name).to.be.a('string');
                        expect(json.surname).to.be.a('string');
                        expect(json.email).to.be.a('string');
                        expect(json.username).to.be.a('string');

                        expect(json.name).to.equal(userTest.name);
                        expect(json.surname).to.equal(userTest.surname);
                        expect(json.email).to.equal(userTest.email);
                        expect(json.username).to.equal(userTest.username);

						done();
					}
				});
		});
	})
};
