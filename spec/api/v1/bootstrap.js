/**
 * Created by alex on 26/04/16.
 */
var global = require('../../helpers/global-v1');

global['userTest'] = {
	name : "test",
	surname : "test",
	email : "test@test.com",
    username : "test",
	password: 'test'
};

global['userTest2'] = {
    name : "test2",
    surname : "test2",
    email : "test2@test.com",
    username : "test2",
    password: 'test2'
};

global['userTest3'] = {
    name : "test3",
    surname : "test3",
    email : "test3@test.com",
    username : "test3",
    password: 'test3'
};

global['favoriteLocationTest'] = {
    as: "AS20044 S/A ESTADO DE MINAS",
    city: "Belo Horizonte",
    country: "Brazil",
    countryCode: "BR",
    isp: "S/a Estado De Minas",
    lat: -19.9191,
    lon: -43.9386,
    org: "S/a Estado De Minas",
    query: "200.188.178.56",
    region: "MG",
    regionName: "Minas Gerais",
    status: "success",
    timezone: "America/Sao_Paulo",
    zip: "",
    hostname: "https://www.google.com.br"
};
