# Avenue Code Geolocation

This is the version 1.0 of Avenue Code Geolocation made by Alex Roberto Corrêa. This project uses Google Maps api that 
allows you know where you are (estimated location), where a website is located and favorite a website.

## Getting Started

To use Avenue Code Geolocation, you'll need:

- You need have access permission the project _avenuecodegeolocation_ in [Bitbucket](https://bitbucket.org/)  
- Download the package that can be done `git clone https://AlexRobertoCorrea@bitbucket.org/AlexRobertoCorrea/avenuecodegeolocation.git`
- Install [MongoDB](https://docs.mongodb.org/manual/installation/) and [NodeJS version 0.12.7](https://nodejs.org/en/blog/release/v0.12.7/)  
- Use the command line (if you are using GNU/Linux) `cd avenuecodegeolocation`
- Install the dependences: `sudo npm install` 
- Install [Grunt Cli](https://www.npmjs.com/package/grunt-cli) witn `sudo npm install -g grunt-cli`
- Install [Bower](http://bower.io/): `sudo npm install -g bower` and run: `bower install`
 
## Tutorial

Now is possible access the website. So, you have two options: assuming you are in the main project folder, run `node server.js` 
and access `http://localhost:3002/` or you can run a fast version doing `grunt serve:dist` and access `http://localhost:3000/`.
If you'd like to run tests, run `npm test`.

The website is very intuitive, you need create a user and login. Next, you can see the favorited websites or favorite one or more.

## How it was made

The Avenue Code Geolocation was made using NodeJS, Express framework, AngularJS and MongoDB database in
GNU/Linux dist Ubuntu 16.04 in 26/27 April, 2016. We have decided to use this tools 
because it is easy work with json format using *Javascript* and [Mongoose](http://mongoosejs.com/).
