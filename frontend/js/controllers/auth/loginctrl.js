/**
 * Created by alex on 27/04/16.
 */
'use strict';

angular.module('avenue-code-geolocation')
	.controller('LoginCtrl', ['$scope','$rootScope','$state','$stateParams','AuthService','UserService',
		function ($scope,$rootScope,$state,$stateParams,AuthService,UserService) {
			$scope.user = {};
			$scope.alert=false;
			$scope.saving = false;

			$scope.main = function() {
				AuthService.me().then(function(auth){
					if(!_.isEmpty(auth))
					{
						$scope.auth = auth || false;
						if($scope.auth)
							_getUser($scope.auth.user.id);
					}
				});
			};

			function _getUser(user_id) {
                UserService.get(user_id)
					.then(function(user){
						$scope.user = user;
					}, function() {
					});
			}

			$scope.login=function(user){
				delete $scope.alert;
				if(!user||!user.username||!user.password)
					$scope.alert={prefix: 'Ops!', text:'Fill the username and password!',type:'alert alert-danger'};
				else {
					delete $scope.alert;
					$scope.loading = true;
					AuthService.login(user.username, user.password)
						.then(function(auth) {
							$state.go('favoritelocationlist');
						}, function(res) {
							if (res && res[0] && res[0].code==2)
								$scope.alert={prefix: 'Ops!', text:'That username / password combination is not valid...',type:'alert alert-danger'};
							else
								$scope.alert={prefix: 'Ops!', text:'Unespected error',type:'alert alert-danger'};
							$scope.loading = false;
						});
				}
			};

			$scope.doSignup = function() {
				$scope.saving = true;
				if($scope.user.email!=$scope.user.emailConfirm)
				{
					$scope.alert={prefix: 'Ops!', text:'The emails aren\'t the same.',type:'alert alert-danger'};
					$("body, html").animate({scrollTop: 0}, "normal");
				}
				else
				{
                    UserService.createUser($scope.user)
						.then(function(res) {
							$scope.alert={text:($scope.user.surname?$scope.user.name + " " + $scope.user.surname:$scope.user.name)+" was successfully created!",type:'alert alert-success'};
							$scope.saving = false;
						})
						.catch(function(err){
							$scope.saving = false;
							if(err[0].httpStatus==401)
							{
								var value = '';
                                if(err[0].value==$scope.user.username)
                                    value = 'username';
								if(err[0].value==$scope.user.email)
									value = 'username';
								$scope.alert={prefix: 'Ops!', text:'This '+value+' already exists. Please, try another '+value+'.',type:'alert alert-danger'};
								$("body, html").animate({scrollTop: 0}, "normal");
							}
							else if(err[1]==403)
							{
								if(err[0]=='Invalid email')
									$scope.alert={prefix: 'Ops!', text:'Invalid email. Please, type the correct email.',type:'alert alert-danger'};
								$("body, html").animate({scrollTop: 0}, "normal");
							}
							else
							{
								$scope.alert={prefix: 'Ops!', text:'It wasn\'t possible save data. Please, try again later.',type:'alert alert-danger'};
								$("body, html").animate({scrollTop: 0}, "normal");
							}
						});
				}
			};

			$scope.close = function(){
				$scope.alert = false;
			};

		}]);
