/**
 * Created by alex on 27/04/16.
 */
'use strict';

angular.module('avenue-code-geolocation')
	.controller('FavoriteLocationListCtrl', ['$scope','$rootScope','$state','$stateParams','auth','FavoriteLocationService',
		function ($scope,$rootScope,$state,$stateParams,auth,FavoriteLocationService) {
            $scope.auth = auth;
			//paginação
			$scope.limit = 15;
			$scope.start = 0;
			$scope.currentPage = 1;

			$scope.main = function() {
				_getFavoriteLocation();
			};

			function _getFavoriteLocation() {
				FavoriteLocationService.list({userId: $scope.auth.user.id}, $scope.auth.id)
					.then(function(res) {
						$scope.total = res.headers()['x-list-total'] || 1;
						$scope.pageNumber = Math.ceil(res.headers()['x-list-total'] / $scope.limit) || 1;
						$scope.favoriteLocations = Array.isArray(res.data) ? res.data : [res.data];
					}, function() {

					});
			}

			$scope.deleteFavoriteLocation = function(favoriteLocation) {
				FavoriteLocationService.deleteFavoriteLocation(favoriteLocation.id, $scope.auth.id)
					.then(function(res) {
						_getFavoriteLocation();
						$scope.alert = {type: 'success', msg: "Favorite location "+favoriteLocation.city+" was successfully deleted."};
						$("body, html").animate({scrollTop: 0}, "normal");
					}, function() {
						$scope.alert = {type: 'danger', msg: "Could not delete the favorite location " +favoriteLocation.city+". Try again later."};
						$("body, html").animate({scrollTop: 0}, "normal");
					});
			};

			$scope.close = function() {
				delete $scope.alert;
			};

			$scope.prevPage = function() {
				if ($scope.currentPage > 1) {
					$scope.currentPage = $scope.currentPage - 1;
					$scope.start = ($scope.currentPage - 1) * $scope.limit;
					$scope.selectedAll = false;
					$scope.marginleft = '';
					_getFavoriteLocation();
				}
			};
			$scope.nextPage = function() {
				if ($scope.currentPage < $scope.pageNumber) {
					$scope.currentPage = $scope.currentPage + 1;
					$scope.start = ($scope.currentPage - 1) * $scope.limit;
					$scope.selectedAll = false;
					$scope.marginleft = '';
					_getFavoriteLocation();
				}
			};

		}]);
