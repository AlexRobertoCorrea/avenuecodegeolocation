/**
 * Created by alex on 27/04/16.
 */
angular.module('avenue-code-geolocation')
    .factory('FavoriteLocationService', ['$q','$http','$window','$state', function($q, $http){
        var baseEndpoint='/api/v1/favoriteLocation';
        var canceler;

        function get(id, authId) {
            return $http.get(baseEndpoint+'/'+id, {headers:{'x-auth':authId}}).then(function(res){
                return res.data;
            }, function(res){
                return $q.reject([res.data, res.status]);
            });
        }

        function list(queryParams, authId) {
            if (canceler) canceler.resolve();
            canceler = $q.defer();
            return $http.get(baseEndpoint, {params: queryParams, headers:{'x-auth':authId}}).then(function(res){
                return res;
            }, function(res){
                return $q.reject([res.data, res.status]);
            });
        }

        function put(favoriteLocation, authId) {
            return $http.put(baseEndpoint, favoriteLocation, {headers:{'x-auth':authId}}).then(function(res){
                return res.data;
            }, function(res){
                return $q.reject([res.data, res.status]);
            });
        }

        function deleteFavoriteLocation(id, authId) {
            return $http.delete(baseEndpoint+'/'+id, {headers:{'x-auth':authId}}).then(function(res){
                return res.status;
            }, function(res){
                return $q.reject([res.data, res.status]);
            });
        }

        return {
            get:get,
            list:list,
            put:put,
            deleteFavoriteLocation:deleteFavoriteLocation
        };
    }]);
