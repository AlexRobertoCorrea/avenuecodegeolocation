/**
 * Created by alex on 26/04/16.
 */
angular.module('avenue-code-geolocation')
	.factory('UserService', ['$q','$http','$window','$state', function($q, $http){
		var baseEndpoint='/api/v1/user';

		function createUser(user) {
			return $http.post(baseEndpoint, user).then(function(res){
				return res.data;
			}, function(res){
				return $q.reject([res.data, res.status]);
			});
		}

		function get(id) {
			return $http.get(baseEndpoint+'/'+id).then(function(res){
				return res.data;
			}, function(res){
				return $q.reject([res.data, res.status]);
			});
		}

		return {
            createUser:createUser,
			get:get
		};
	}]);
