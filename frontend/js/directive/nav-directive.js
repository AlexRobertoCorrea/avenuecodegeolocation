/**
 * Created by alex on 26/04/16.
 */
angular.module('avenue-code-geolocation')
	.directive('navMenu', ['$stateParams', function ($stateParams) {
		return {
			templateUrl: 'templates/nav-menu/nav-menu.html',
			scope: true,
			replace: true,
			restrict: 'E',
			controller: ['$scope','$state',
				function($scope,$state) {
					$scope.status = {
						isopen: false
					};

					if($state.current.controller == 'FavoriteLocationListCtrl' || $state.current.controller == 'FavoriteLocationCtrl')
					{
						$scope.leftTab = 'List favorite locations';
						$scope.rightTab = 'Add favorite location';
					}
					if($state.current.controller == 'FavoriteLocationListCtrl')
					{
						$scope.leftActive = 'active';
						$scope.rightActive = '';
					}
					else if($state.current.controller == 'FavoriteLocationCtrl')
					{
						$scope.leftActive = '';
						$scope.rightActive = 'active';
					}

					$scope.nextTab = function() {
						if($state.current.controller == 'FavoriteLocationListCtrl')
							$state.go('favoritelocationnew');
						else if($state.current.controller == 'FavoriteLocationCtrl')
							$state.go('favoritelocationlist');
					}
			}]
		};
	}]);
