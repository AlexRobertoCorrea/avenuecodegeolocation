/**
 * Created by alex on 27/04/16.
 */
var express = require('express');
var _ = require('underscore');
var ERRORS=require('../../../utils/constants').ERRORS;
var favoriteLocationService=require('../../../services/favoriteLocationService');

exports = module.exports = this_module;

function this_module(options) {

    var api = express.Router();

    api.route('/')
        .get(function (req, res) {
            var userId = req.query.userId;
            if(!userId)
            {
                res.status(ERRORS.unauthorized.httpStatus);
            }
            var params = {
                user: userId
            };
            return favoriteLocationService.listAndCount(params).spread(function(favoriteLocations, count) {
                res.header('X-List-Total', count).json(favoriteLocations);
            }, function(err) {
                res.status(ERRORS.unknown.httpStatus).json(_.extend({},ERRORS.unauthorized,{original:err&&err.stack||err}));
                console.error('[favoriteLocation get] error: ',err&&err.stack||err);
            });
        })
        .put(function (req, res) {
            var json = req.body;
            return favoriteLocationService.update(req.params.id, json).then(function(favoriteLocation){
                res.json(favoriteLocation);
            }, function(err) {
                if(err && err.code==11000) {
                    var value = err.err&&err.err.match(/: "([^"]+)"/);
                    res.status(ERRORS.duplicate_value.httpStatus).json(_.extend({},ERRORS.duplicate_value,{value: value&&value[1]}));
                }
                else {
                    res.status(ERRORS.unknown.httpStatus).json(_.extend({},ERRORS.unauthorized,{original:err&&err.stack||err}));
                    console.error('[favoriteLocation/:favoriteLocationId put] error: ',err&&err.stack||err);
                }
            });
        });
    api.route('/:id')
        .get(function (req, res) {
            return favoriteLocationService.get(req.params.id).then(function(favoriteLocation) {
                res.json(favoriteLocation);
            }, function(err) {
                res.status(ERRORS.unknown.httpStatus).json(_.extend({},ERRORS.unauthorized,{original:err&&err.stack||err}));
                console.error('[favoriteLocation/:favoriteLocationId get] error: ',err&&err.stack||err);
            });
        })
        .delete(function (req, res) {
            return favoriteLocationService.delete(req.params.id).then(function() {
                res.status(204).json("Favorite Location successfully deleted.");
            }, function(err) {
                res.status(ERRORS.unknown.httpStatus).json(_.extend({},ERRORS.unauthorized,{original:err&&err.stack||err}));
                console.error('[favoriteLocation/:favoriteLocationId delete] error: ',err&&err.stack||err);
            });
        });

    return api;
}
