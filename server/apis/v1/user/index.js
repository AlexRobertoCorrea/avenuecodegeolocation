/**
 * Created by alex on 26/04/16.
 */
var express = require('express');
var _ = require('underscore');
var ERRORS=require('../../../utils/constants').ERRORS;
var crypter=require('../../../utils/crypter');
var userService=require('../../../services/userService');

exports = module.exports = this_module;

function this_module(options) {

	var api = express.Router();

	api.route('/')
		.post(function(req, res) {
			var json = req.body;
			if(json.password)
				json.password = crypter.hash(req.body.password);
			return userService.create(json).then(function(user) {
				res.json(user);
			}).catch(function(err) {
				if(err && err.code==11000) {
					var value = err.err&&err.err.match(/: "([^"]+)"/);
					res.status(ERRORS.duplicate_value.httpStatus).json(_.extend({},ERRORS.duplicate_value,{value: value&&value[1]}));
				} else {
					if(err&&err.errors&&err.errors&&err.errors.email&&err.errors.email.message)
						res.status(ERRORS.unauthorized.httpStatus).json(err.errors.email.message);
					else
						res.status(ERRORS.unknown.httpStatus).json(_.extend({},ERRORS.unauthorized,{original:err&&err.stack||err}));
					console.error('[user post] error: ',err&&err.stack||err);
				}
			}).done();
		});
	api.route('/:id')
		.get(function (req, res) {
			return userService.get(req.params.id).then(function(user) {
				res.json(user);
			}, function(err) {
				res.status(ERRORS.unknown.httpStatus).json(_.extend({},ERRORS.unauthorized,{original:err&&err.stack||err}));
				console.error('[user/:userId get] error: ',err&&err.stack||err);
			});
		});

	return api;
}
