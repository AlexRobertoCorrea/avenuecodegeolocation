/**
 * Created by alex on 26/04/16.
 */
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var mongoose_plugins = require('../utils/mongoose_plugins');
var helpers = require('../utils/helpers');

var UserSchema   = new Schema({
	name: {type: String, required: true, trim:true},
	surname: String,
	password: {type:String, required: true},
	email: {type: String, required: true, unique:true, trim:true},
    username: {type: String, index: true, unique:true, sparse: true}
});
UserSchema.plugin(mongoose_plugins.timestamps);

UserSchema.path('email').validate(function (value) {
	return helpers.verifyEmail(value);
}, 'Invalid email');

//Transform
UserSchema.set('toJSON', {
	virtuals: true,
	transform: function (doc, ret, options) {
		delete ret._id;
		delete ret.__v;
		delete ret.password;
	}
});

var User = module.exports = mongoose.model('User', UserSchema);
