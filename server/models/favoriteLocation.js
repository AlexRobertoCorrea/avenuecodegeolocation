/**
 * Created by alex on 27/04/16.
 */
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var mongoose_plugins = require('../utils/mongoose_plugins');
var relationship = require("mongoose-relationship");

var FavoriteLocationSchema   = new Schema({
    user: { type:Schema.ObjectId, ref:"User", childPath:"favoriteLocations" },
    as: String,
    city: String,
    country: String,
    countryCode:String,
    isp: String,
    lat: Number,
    lon: Number,
    org: String,
    query: String,
    region: String,
    regionName: String,
    status: String,
    timezone: String,
    zip: String,
    hostname: String
});
FavoriteLocationSchema.plugin(mongoose_plugins.timestamps);

// Parent Relations
FavoriteLocationSchema.plugin(relationship, { relationshipPathName:['user'] });

//Transform
FavoriteLocationSchema.set('toJSON', {
    virtuals: true,
    transform: function (doc, ret, options) {
        delete ret._id;
        delete ret.__v;
    }
});

var FavoriteLocation = module.exports = mongoose.model('FavoriteLocation', FavoriteLocationSchema);
