/**
 * Created by alex on 26/04/16.
 */
var Client = require('../models/user');
var baseService = require('./baseService');

module.exports.create=function(json) {
	return baseService.create(Client,json).then(function(client) {
		return client;
	});
};

module.exports.get=function(id, populates) {
	return baseService.get(Client, {_id: id}, populates).then(function(client) {
		return client;
	});
};
