/**
 * Created by alex on 27/04/16.
 */
var FavoriteLocation = require('../models/favoriteLocation');
var baseService = require('./baseService');

module.exports.get=function(id, populates) {
    return baseService.get(FavoriteLocation, {_id: id}, populates).then(function(favoriteLocation) {
        return favoriteLocation;
    });
};

module.exports.listAndCount=function(params, start,limit, sort, populates) {
    return baseService.listAndCount(FavoriteLocation,params,start,limit, sort, populates);
};

module.exports.update=function(id, json) {
    return baseService.createOrUpdate(FavoriteLocation, {_id: id}, json).then(function(favoriteLocation) {
        return favoriteLocation;
    });
};

module.exports.delete=function(id) {
    return baseService.delete(FavoriteLocation, {_id: id});
};
