/**
 * Created by alex on 27/04/16.
 */
'use strict';

describe('Service: FavoriteLocationService', function () {

    // load the service's module
    beforeEach(module('avenue-code-geolocation'));

    // instantiate service
    var FavoriteLocationService;
    beforeEach(inject(function (_FavoriteLocationService_) {
        FavoriteLocationService = _FavoriteLocationService_;
    }));

    it('should do something', function () {
        expect(!!FavoriteLocationService).toBe(true);
    });

});
