/**
 * Created by alex on 27/04/16.
 */
'use strict';

describe('Service: UserService', function () {
	var UserService;

	// load the service's module
	beforeEach(module('avenue-code-geolocation'));

	beforeEach(inject(function (_UserService_) {
        UserService = _UserService_;
	}));

	it('should do something', function () {
		expect(!!UserService).toBe(true);
	});

});
