/**
 * Created by alex on 27/04/16.
 */
describe('LoginCtrl', function() {
	beforeEach(module('avenue-code-geolocation'));

	var $controller;
    var $rootScope;
    var $state;
    var $stateParams;
    var AuthService;
    var UserService;

    beforeEach(inject(function($injector) {
        $controller = $injector.get('$controller');
        $rootScope = $injector.get('$rootScope');
        $state = $injector.get('$state');
        $stateParams = $injector.get('$stateParams');
        AuthService = $injector.get('AuthService');
        UserService = $injector.get('UserService');
    }));

	describe('$scope.close', function() {
		it('Cancel the alert', function() {
            var $scope = {};
			var controller = $controller('LoginCtrl', { $scope: $scope });
			$scope.alert = {};
			$scope.close();
			expect($scope.alert).toEqual(false);
		});
	});
});
