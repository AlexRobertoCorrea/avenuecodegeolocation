/**
 * Created by alex on 27/04/16.
 */
'use strict';
describe('FavoriteLocationCtrl', function() {
	var $controller;
    var auth;

    beforeEach(function() {
        module('avenue-code-geolocation');

        module(function($provide) {

            $provide.value('auth', {
                auth: function() {
                    return {};
                }
            });
            return null;
        });
    });

    beforeEach(inject(function($injector) {
        $controller = $injector.get('$controller');
    }));

	describe('$scope.close', function() {
		it('Cancel the alert', function() {
			var $scope = {};
			var controller = $controller('FavoriteLocationCtrl', {
                $scope: $scope
            });
			$scope.alert = {};
			$scope.close();
			expect($scope.alert).toEqual(undefined);
		});
	});
});
